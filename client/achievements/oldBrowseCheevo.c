void browseAchievements()
{
	float idx, x,y;
	float hWidth, nWidth, dWidth;

	drawfill('0 0', screen_size, '0 0 0', 0.65, 0);
	drawstring('50 50',"Achievements: ", '16 16', '0.8 0.8 0.8', 1, 0);

	vector cheevSize, cheevPos, descPos, bgPos, bgSize;
	cheevSize = '50 50';	//update source image
	cheevPos = '0 0 0';
	bgSize = cheevSize*2;	//something seems wrong here. but it checks out visually *scratches head*

	idx = 1;
	for(y = 0; y < 4; y++)
	{
		for(x = 0; x < 8; x++)
		{
			cheevPos_x = 100 + x*(cheevSize_x + 30);
			cheevPos_y = 100 + y*(cheevSize_y + 30);

			bgPos = cheevPos - (cheevSize/2);

			drawpic(bgPos, "gfx/bigbox", bgSize, '1 1 1', 0.6, 0);

			if(idx > achievementCount)
				continue;

			drawpic(cheevPos, cheevoIcons(idx), cheevSize, '1 1 1', 1, 0);

			if(!(isCompletedCheevo(idx)))
				if(isSecretCheevo(idx))
					drawpic(cheevPos, "gfx/secret", cheevSize, '1 1 1', 1, 0);

			idx++;
		}
	}

	//need to loop over description draws, because if i do it in the main drawloop, it covers up the drawstrings
	idx = 1;
	for(y = 0; y < 4; y++)
	{
		for(x = 0; x < 8; x++)
		{
			cheevPos_x = 100 + x*(cheevSize_x + 30);
			cheevPos_y = 100 + y*(cheevSize_y + 30);

			bgPos = cheevPos - (cheevSize/2);	//position of the background tiles

			nWidth = stringwidth(cheevoNames(idx), 0, '14 14');
			dWidth = stringwidth(cheevoDesc(idx), 0, '8 8');

			hWidth = (nWidth > dWidth) ? nWidth : dWidth;

			if(pointInBounds(mpos, cheevPos, cheevPos+cheevSize))
			{
				descPos = bgPos + [0, bgSize_y];

				if((descPos_x + hWidth + 5) > (screen_size_x))		//THIS AND THE LINE BENEATH NEED TO BE UPDATED.
					descPos_x = (bgPos_x + bgSize_x) - hWidth;

				drawfill(descPos + '-2.5, 2.5', [hWidth + 5, 30], '0 0 0', 1, 0);

				drawstring(descPos, cheevoNames(idx), '14 14', '0.8 0.8 0.8', 1, 0);
				drawstring(descPos + '0 18', cheevoDesc(idx), '8 8', '0.8 0.8 0.8', 1, 0);
			}

			idx++;
		}
	}
}
